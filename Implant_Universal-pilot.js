// New Format Universal-pilot v.1.0

//Universal-pilot (Getting input)
var drillingPathType = logicData.getDrillinPathType();
var guideType = logicData.getGuideType();
var implantLength = logicData.getImplantLength();
var implantOcclusal = logicData.getImplantOcclusalDiameter();
var depthConrtol = logicData.IsDepthControlOn();
var familyName = logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;
var implantApical = logicData.getImplantApicalDiameter();


//Logic (Setting output)
var SleeveLength = 4.0;
var SleeveThickness = 0.6;
var SleeveShelfHeight = 0.25;
var SleeveShelfWidth = 0.25;
var AddedMaterialThickness = 2.0;
var AddDrillLength = 0.0;
var DrillPathDiameter = 2.1;
SleeveInnerDiameter = DrillPathDiameter;



//  Setting different parameters

var offset1 = 11 - implantLength;
var offset2 = 14 - implantLength;
var offset3 = 16 - implantLength;
var offset4 = 19 - implantLength;
var offset5 = 21 - implantLength;
var offset6 = 24 - implantLength;

var offsets = new Array(offset1, offset2, offset3, offset4, offset5, offset6);
var offsets_final = new Array();
var tools = new Array();

print (offsets);

// Set offsets

var i;
for (i = 0; i < offsets.length; i++) {
    if (offsets[i] > 0) {
        offsets_final.push(offsets[i]);  
        tools.push(0);      
    } 
}

// if all values are negative, the arrays would be empty so add (0) as the default value

if (offsets_final.length == 0 || tools.length == 0) {
    offsets_final[0] = 0;
    tools[0] = 0;
}

print("printing final arrays:");
print(offsets_final);
print(tools);


setSleeveOffsets(offsets_final, tools); // The magic happens here



//Setting Output in the logic object to be used in the application
logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(DrillPathDiameter);
logicData.setAddDrillLength(AddDrillLength);
logicData.setSucceedStatus(true);

// Equality function.
function IsEqual(value, diameter) { // I use toFixed function to round the decimal value to two places.
    value = value.toFixed(2);
    diameter = diameter.toFixed(2);
    return (value - diameter) == (0.0).toFixed(2);
}

// Set DrillPath parameters in one func call.
function setDrillPathParams(addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength) { // Repeated logic added in a function.
    AddDrillLength = addDrill;
    DrillPathDiameter = dPDiameter;
    SleeveInnerDiameter = DrillPathDiameter;
    SleeveShelfWidth = ShWidth;
    SleeveShelfHeight = ShHeight;
    SleeveThickness = SlThickness;
    SleeveLength = SlLength;

    print("Add drill length");
    print(AddDrillLength);
}

// Set offsets in one function call.
function setSleeveOffsets(offsets, tools) {
    for (i = 0; i < offsets.length; i++) {
        logicData.addSleeveOffsetAndTool(offsets[i], tools[i]);
    }
}
