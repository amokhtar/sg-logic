// New Format v.1.0

//Pilot-Zimmer Logic (Getting input)
var drillingPathType = logicData.getDrillinPathType();
var guideType = logicData.getGuideType();
var implantLength = logicData.getImplantLength();
var implantOcclusal = logicData.getImplantOcclusalDiameter();
var depthConrtol = logicData.IsDepthControlOn();
var familyName = logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;
var implantApical = logicData.getImplantApicalDiameter();


//Logic (Setting output)
var SleeveLength = 1.0;
var SleeveThickness = 1.0;
var SleeveShelfHeight = 0.0;
var SleeveShelfWidth = 0.0;
var AddedMaterialThickness = 1.0;
var AddDrillLength = 0.0;
var DrillPathDiameter = 1.0;

var stri = familyName.toString().toLowerCase();

// Family name array
var families = [];

//  Setting different parameters
//  addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength


if (IsEqual(3.50, implantOcclusal)) {
    print("D = 3.50");

    
}
else if (IsEqual(3.70, implantOcclusal)) {
    print("D = 3.7");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.75, 4.75, 7.75, 10.75], [0.0, 0.0, 0.0, 0.0]);
    }

    else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75, 8.75], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.25, 4.25, 7.25], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75], [0.0, 0.0]);
    }

    else if (IsEqual(16.0, implantLength)) {
        print("L = 16.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75], [0.0]);
    }
}
else if (IsEqual(3.90, implantOcclusal)) {
    print("D = 3.90");

    
}

else if (IsEqual(4.0, implantOcclusal) || IsEqual(4.10, implantOcclusal)) {
    print("D = 4.1");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.75, 4.75, 7.75, 10.75], [0.0, 0.0, 0.0, 0.0]);
    }

    else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75, 8.75], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.25, 4.25, 7.25], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75], [0.0, 0.0]);
    }

    else if (IsEqual(16.0, implantLength)) {
        print("L = 16.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75], [0.0]);
    }
}

else if (IsEqual(4.70, implantOcclusal)) {
    print("D = 4.5 and 4.7");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.75, 4.75, 7.75, 10.75], [0.0, 0.0, 0.0, 0.0]);
    }

    else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75, 8.75], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.25, 4.25, 7.25], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75], [0.0, 0.0]);
    }

    else if (IsEqual(16.0, implantLength)) {
        print("L = 16.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75], [0.0]);
    }
}

else if (IsEqual(5.70, implantOcclusal) || IsEqual(5.90, implantOcclusal) || IsEqual(6.0, implantOcclusal)) {
    print("D = 5.7 and 6.0");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.75, 4.75, 7.75, 10.75], [0.0, 0.0, 0.0, 0.0]);
    }

    else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75, 8.75], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([1.25, 4.25, 7.25], [0.0, 0.0, 0.0]);
    }

    else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75, 5.75], [0.0, 0.0]);
    }

    else if (IsEqual(16.0, implantLength)) {
        print("L = 16.0");

        setDrillPathParams(1.25, 2.41, 0.0, 0.0, 0.57, 5.0);
        setSleeveOffsets([2.75], [0.0]);
    }

} else {
    print("Not in Library!");
}



//Setting Output in the logic object to be used in the application
logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(DrillPathDiameter);
logicData.setAddDrillLength(AddDrillLength);
logicData.setSucceedStatus(true);

// Equality function.
function IsEqual(value, diameter) { // I use toFixed function to round the decimal value to two places.
    value = value.toFixed(2);
    diameter = diameter.toFixed(2);
    return (value - diameter) == (0.0).toFixed(2);
}

// Set DrillPath parameters in one func call.
function setDrillPathParams(addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength) { // Repeated logic added in a function.
    AddDrillLength = addDrill;
    DrillPathDiameter = dPDiameter;
    SleeveInnerDiameter = DrillPathDiameter;
    SleeveShelfWidth = ShWidth;
    SleeveShelfHeight = ShHeight;
    SleeveThickness = SlThickness;
    SleeveLength = SlLength;

    print("Add drill length");
    print(AddDrillLength);
}

// Set offsets in one function call.
function setSleeveOffsets(offsets, tools) {
    for (i = 0; i < offsets.length; i++) {
        logicData.addSleeveOffsetAndTool(offsets[i], tools[i]);
    }
}
