// New Format v.1.1

//General Fixation Screw Logic (Getting input)
var guideType 					= logicData.getGuideType();
var fixationScrewLength 		= logicData.getImplantLength();
var fixationScrewOcclusal 		= logicData.getImplantOcclusalDiameter();
var depthConrtol				= logicData.IsDepthControlOn();
var familyName					= logicData.getFamilyName();
var SleeveInnerDiameter 		= fixationScrewOcclusal;

//Logic (Setting output)
var SleeveLength = 11.0;
var SleeveThickness = 0.74;
var SleeveShelfHeight = 0.0;
var SleeveShelfWidth = 0.0;
var AddedMaterialThickness = 1.0;
var SleeveInnerDiameter = 1.65;

// No sleeve offset for fixation screw just apply the default
//logicData.addSleeveOffsetAndTool(6.0, 0.0);

//Setting Output in the logic object to be used in the application
logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(fixationScrewOcclusal);
logicData.setSucceedStatus(true);
