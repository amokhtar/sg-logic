// New Format v.1.2

//Biomet 3i Logic (Getting input)
var drillingPathType	= logicData.getDrillinPathType();
var guideType 			= logicData.getGuideType();
var implantLength 		= logicData.getImplantLength();
var implantOcclusal 	= logicData.getImplantOcclusalDiameter();
var depthConrtol		= logicData.IsDepthControlOn();
var familyName			= logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;



//Logic (Setting output)
var SleeveLength = 4.0;
var SleeveThickness = 0.58;
var SleeveShelfHeight = 0.25;
var SleeveShelfWidth = 0.2;
var AddedMaterialThickness = 1.0;
var AddDrillLength = 0.0;
var DrillPathDiameter = 4.2;
var AddTool = 1.7;

var stri = familyName.toString().toLowerCase();

//print("hello");

// print(stri);

// Logic for Tapred implants

//if ((stri.indexOf("tapered") > -1)) {
 //   print("=> the implant is tapered");
// 	Setting different parameters

// 	Logic for implant diameters 3.25 and 4.1 are similar
if ((IsEqual(3.30, implantOcclusal)) || (IsEqual(4.0, implantOcclusal))) {

    print("D: 3.30 or D: 4.0");

    setDrillPathParams(0, 4.2, 0.2, 0.3, 0.58, 4.0);

} else if (IsEqual(5.0, implantOcclusal)) { //// Logic for implant diameters 5.0 is different

    setDrillPathParams(0, 5.15, 0.2, 0.3, 0.6, 4.0);

} else if (IsEqual(6.0, implantOcclusal)) { // Logic for implant diameters 6.0

    // No logic
}


// Logic for non-tapered implants
    //} 
 //   else {
 ////   print("=> the implant is NOT tapered");
 //   SleeveLength = 4.0;
 //   SleeveThickness = 0.58;
 //   SleeveShelfHeight = 0.25;
 //   SleeveShelfWidth = 0.2;
 //   SleeveInnerDiameter = 4.2;
//}

// print("SleeveLength = ", SleeveLength);
 


// Set the Drill path diameter after setting the sleeve inner diameter which are similar
//DrillPathDiameter = SleeveInnerDiameter;





// setting sleeve offsets

setSleeveOffsets([5.0], [1.5]);
setSleeveOffsets([8.0], [1.5]);



//Setting Output in the logic object to be used in the application
logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(DrillPathDiameter);
logicData.setAddDrillLength(AddDrillLength);
logicData.setSucceedStatus(true);
logicData.setToolThickness(AddTool);


// Equality function.
function IsEqual(value, diameter) { // I use toFixed function to round the decimal value to two places.
    value = value.toFixed(2);
    diameter = diameter.toFixed(2);
    return (value - diameter) == (0.0).toFixed(2);
}

// Set DrillPath parameters in one func call.
function setDrillPathParams(addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength) { // Repeated logic added in a function.
    AddDrillLength = addDrill;
    DrillPathDiameter = dPDiameter;
    SleeveInnerDiameter = DrillPathDiameter;
    SleeveShelfWidth = ShWidth;
    SleeveShelfHeight = ShHeight;
    SleeveThickness = SlThickness;
    SleeveLength = SlLength;

    print("sleeve inner diameter:");
    print(SleeveInnerDiameter);
}

// Set offsets in one function call.
function setSleeveOffsets(offsets, tools) {
    for (i = 0; i < offsets.length; i++) {
        logicData.addSleeveOffsetAndTool(offsets[i], tools[i]);
    }
}