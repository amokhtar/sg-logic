// New Format v.1.1

//Straumann Logic (Getting input)
var drillingPathType = logicData.getDrillinPathType();
var guideType = logicData.getGuideType();
var implantLength = logicData.getImplantLength();
var implantOcclusal = logicData.getImplantOcclusalDiameter();
var depthConrtol = logicData.IsDepthControlOn();
var familyName = logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;
var implantApical = logicData.getImplantApicalDiameter();


//Logic (Setting output)
var SleeveLength = 1.0;
var SleeveThickness = 1.0;
var SleeveShelfHeight = 0.0;
var SleeveShelfWidth = 0.0;
var AddedMaterialThickness = 1.0;
var AddDrillLength = 0.0;
var DrillPathDiameter = 1.0;
var AddTool = 0.0;

var stri = familyName.toString().toLowerCase();

// Family name array
var families = [];

// 	Setting different parameters


if (IsEqual(3.3, implantApical)) {
    print("D = 3.3");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [1.0, 3.0, 1.0]);

        printDrillDepthLength(3.0);
    }

    else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [3.0, 1.0, 3.0]);
    }

    else if (IsEqual(12.0, implantLength)) {
        print("L = 12.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [1.0, 3.0, 1.0]);
    }

    else if (IsEqual(14.0, implantLength)) {
        print("L = 14.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0], [3.0, 1.0]);
    }
   
}

else if (IsEqual(4.10, implantApical)) {
    print("D = 4.1");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [1.0, 3.0, 1.0]);
    }

    else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [3.0, 1.0, 3.0]);
    }

    else if (IsEqual(12.0, implantLength)) {
        print("L = 12.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [1.0, 3.0, 1.0]);
    }

    else if (IsEqual(14.0, implantLength)) {
        print("L = 14.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0], [3.0, 1.0]);
    }
    
}

else if (IsEqual(4.80, implantApical)) {
    print("D = 4.8");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [1.0, 3.0, 1.0]);
    }

    else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [3.0, 1.0, 3.0]);
    }

    else if (IsEqual(12.0, implantLength)) {
        print("L = 12.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0, 6.0], [1.0, 3.0, 1.0]);
    }

    else if (IsEqual(14.0, implantLength)) {
        print("L = 14.0");

        setDrillPathParams(0, 5.02, 0.20, 1.30, 0.55, 5.0);
        setSleeveOffsets([2.0, 4.0], [3.0, 1.0]);
    }
}

else {
    print("Not in Library!");
}



//Setting Output in the logic object to be used in the application
logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(DrillPathDiameter);
logicData.setAddDrillLength(AddDrillLength);
logicData.setSucceedStatus(true);
logicData.setToolThickness(AddTool);

// Equality function.
function IsEqual(value, diameter) { // I use toFixed function to round the decimal value to two places.
    value = value.toFixed(2);
    diameter = diameter.toFixed(2);
    return (value - diameter) == (0.0).toFixed(2);
}

// Set DrillPath parameters in one func call.
function setDrillPathParams(addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength) { // Repeated logic added in a function.
    AddDrillLength = addDrill;
    DrillPathDiameter = dPDiameter;
    SleeveInnerDiameter = DrillPathDiameter;
    SleeveShelfWidth = ShWidth;
    SleeveShelfHeight = ShHeight;
    SleeveThickness = SlThickness;
    SleeveLength = SlLength;

    print("Add tool");
    print(AddTool);
    print("Add drill length");
    print(AddDrillLength);
}

// Set offsets in one function call.
function setSleeveOffsets(offsets, tools) {
    for (i = 0; i < offsets.length; i++) {
        logicData.addSleeveOffsetAndTool(offsets[i], tools[i]); 
    }
}

function printDrillDepthLength(offset) {
    print("Drill depth Length:");
    print(implantLength + SleeveLength + offset + AddTool + AddDrillLength);
}
