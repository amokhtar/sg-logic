// New Format v1.1

//General Logic (Getting input)
var drillingPathType	= logicData.getDrillinPathType();
var guideType 			= logicData.getGuideType();
var implantLength 		= logicData.getImplantLength();
var implantOcclusal 	= logicData.getImplantOcclusalDiameter();
var depthConrtol		= logicData.IsDepthControlOn();
var familyName			= logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;

//Logic (Setting output)
var SleeveLength = 4.00;
var SleeveThickness = 0.6;
var SleeveShelfHeight = 0.2500;
var SleeveShelfWidth = 0.2500;
var AddedMaterialThickness = 1.0;
var AddDrillLength = 0.0;

SleeveInnerDiameter = 2.1

//Setting Output in the logic object to be used in the application
logicData.addSleeveOffsetAndTool(6.0, 0.0);

logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(implantOcclusal);
logicData.setAddDrillLength(AddDrillLength);
logicData.setSucceedStatus(true);
