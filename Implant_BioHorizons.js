// New Format v.1.2

//BioHorizons Logic (Getting input)
var drillingPathType = logicData.getDrillinPathType();
var guideType = logicData.getGuideType();
var implantLength = logicData.getImplantLength();
var implantOcclusal = logicData.getImplantOcclusalDiameter();
var depthConrtol = logicData.IsDepthControlOn();
var familyName = logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;
var implantApical = logicData.getImplantApicalDiameter();


//Logic (Setting output)
var SleeveLength = 1.0;
var SleeveThickness = 1.0;
var SleeveShelfHeight = 0.0;
var SleeveShelfWidth = 0.0;
var AddedMaterialThickness = 1.0;
var AddDrillLength = 0.0;
var DrillPathDiameter = 1.0;
var AddTool = 0.0;

var stri = familyName.toString().toLowerCase();

// Family name array
var families = [];

// 	Setting different parameters


if (IsEqual(3.0, implantOcclusal)) {
    print("D = 3.0");

    if (IsEqual(7.50, implantLength)) {
        print("L = 7.5");

        // No Logic

    }

    else if (IsEqual(9.0, implantLength)) {
        print("L = 9.0");

        // No Logic
    }

    else if (IsEqual(10.5, implantLength)) {
        print("L = 10.5");

        setDrillPathParams(0, 4.3, 0.40, 0.50, 0.50, 6.0);
        setSleeveOffsets([3.0, 6.0], [1.50, 1.50]);
    }

    else if (IsEqual(12.0, implantLength)) {
        print("L = 12.0");

        setDrillPathParams(0, 4.3, 0.40, 0.50, 0.50, 6.0);
        setSleeveOffsets([4.5], [1.50]);
    }

    else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0, 4.3, 0.40, 0.50, 0.50, 6.0);
        setSleeveOffsets([5.5], [1.50]);
    }
   
}

else if (IsEqual(3.80, implantOcclusal)) {
    print("D = 3.8");

    if (IsEqual(7.50, implantLength)) {
        print("L = 7.5");

        // No Logic

    }

    else if (IsEqual(9.0, implantLength)) {
        print("L = 9.0");

        setDrillPathParams(0, 4.4, 0.40, 0.50, 0.50, 6.0);
        setSleeveOffsets([4.5], [1.50]);
    }

    else if (IsEqual(10.5, implantLength)) {
        print("L = 10.5");

        setDrillPathParams(0, 4.4, 0.40, 0.50, 0.50, 6.0);
        setSleeveOffsets([3.0, 6.0], [1.50, 1.50]);
    }

    else if (IsEqual(12.0, implantLength)) {
        print("L = 12.0");

        setDrillPathParams(0, 4.4, 0.40, 0.50, 0.50, 6.0);
        setSleeveOffsets([4.5], [1.50]);
    }

    else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0, 4.4, 0.40, 0.50, 0.50, 6.0);
        setSleeveOffsets([5.5], [1.50]);
    }
    
}

else if (IsEqual(4.60, implantOcclusal)) {
    print("D = 4.6");

    if (IsEqual(7.50, implantLength)) {
        print("L = 7.5");

        setDrillPathParams(0, 5.2, 0.3, 0.50, 0.50, 6.0);
        setSleeveOffsets([2.0, 6.0], [1.50, 1.50]);

    }

    else if (IsEqual(9.0, implantLength)) {
        print("L = 9.0");

        setDrillPathParams(0, 5.2, 0.3, 0.50, 0.50, 6.0);
        setSleeveOffsets([4.5], [1.50]);
    }

    else if (IsEqual(10.5, implantLength)) {
        print("L = 10.5");

        setDrillPathParams(0, 5.2, 0.3, 0.50, 0.50, 6.0);
        setSleeveOffsets([3.0, 6.0], [1.50, 1.50]);
    }

    else if (IsEqual(12.0, implantLength)) {
        print("L = 12.0");

        setDrillPathParams(0, 5.2, 0.3, 0.50, 0.50, 6.0);
        setSleeveOffsets([4.5], [1.50]);
    }

    else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0, 5.2, 0.3, 0.50, 0.50, 6.0);
        setSleeveOffsets([5.5], [1.50]);
    }
}

else if (IsEqual(5.80, implantOcclusal)) {
    print("D = 5.8");

    if (IsEqual(7.50, implantLength)) {
        print("L = 7.5");

        setDrillPathParams(0, 6.4, 0.60, 0.55, 0.5, 6.0);
        setSleeveOffsets([2.0, 6.0], [1.50, 1.50]);

    }

    else if (IsEqual(9.0, implantLength)) {
        print("L = 9.0");

        setDrillPathParams(0, 6.4, 0.60, 0.55, 0.5, 6.0);
        setSleeveOffsets([4.5], [1.50]);
    }

    else if (IsEqual(10.5, implantLength)) {
        print("L = 10.5");

        setDrillPathParams(0, 6.4, 0.60, 0.55, 0.5, 6.0);
        setSleeveOffsets([3.0, 6.0], [1.50, 1.50]);
    }

    else if (IsEqual(12.0, implantLength)) {
        print("L = 12.0");

        setDrillPathParams(0, 6.4, 0.60, 0.55, 0.5, 6.0);
        setSleeveOffsets([4.5], [1.50]);
    }

    else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0, 6.4, 0.60, 0.55, 0.5, 6.0);
        setSleeveOffsets([5.5], [1.50]);
    }
}

else {
    print("Not in Library!");
}



//Setting Output in the logic object to be used in the application
logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(DrillPathDiameter);
logicData.setAddDrillLength(AddDrillLength);
logicData.setSucceedStatus(true);
logicData.setToolThickness(AddTool);


// Equality function.
function IsEqual(value, diameter) { // I use toFixed function to round the decimal value to two places.
    value = value.toFixed(2);
    diameter = diameter.toFixed(2);
    return (value - diameter) == (0.0).toFixed(2);
}

// Set DrillPath parameters in one func call.
function setDrillPathParams(addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength) { // Repeated logic added in a function.
    AddDrillLength = addDrill;
    DrillPathDiameter = dPDiameter;
    SleeveInnerDiameter = DrillPathDiameter;
    SleeveShelfWidth = ShWidth;
    SleeveShelfHeight = ShHeight;
    SleeveThickness = SlThickness;
    SleeveLength = SlLength;

    print("Add drill length");
    print(AddDrillLength);
}

// Set offsets in one function call.
function setSleeveOffsets(offsets, tools) {
    for (i = 0; i < offsets.length; i++) {
        logicData.addSleeveOffsetAndTool(offsets[i], tools[i]);
    }
}

function printDrillDepthLength(offset) {
    print("Drill depth Length:");
    print(implantLength + SleeveLength + offset + AddTool + AddDrillLength);
}
