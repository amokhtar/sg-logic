// New Format v.1.3

//Nobel Logic (Getting input)
var drillingPathType	= logicData.getDrillinPathType();
var guideType 			= logicData.getGuideType();
var implantLength 		= logicData.getImplantLength();
var implantOcclusal 	= logicData.getImplantOcclusalDiameter();
var depthConrtol		= logicData.IsDepthControlOn();
var familyName			= logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;
var implantApical = logicData.getImplantApicalDiameter();


//Logic (Setting output)
var SleeveLength = 1.0;
var SleeveThickness = 1.0;
var SleeveShelfHeight = 0.0;
var SleeveShelfWidth = 0.0;
var AddedMaterialThickness = 1.0;
var AddDrillLength = 0.0;
var DrillPathDiameter = 1.0;
var AddTool = 0.0;

var stri = familyName.toString().toLowerCase();

// Family name array
var families = [];


// 	Setting different parameters

//if ((stri.indexOf("straight") > -1)) {
//    print("=> Tapered Implant");
//}

//print("Straight");
print("\n");

// addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength

if (IsEqual(3.0, implantOcclusal)) {
    print("D = 3.0");

    if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.0, 8.5, 10.5, 13.5], [1.0, 1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.0, 9.0, 12.0], [1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.5, 10.5], [1.0, 1.0, 1.0]);

    } else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 8.5], [1.0, 1.0]);

    } else if (IsEqual(18.0, implantLength)) {
        print("L = 18.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5], [1.0]);

    }
} else if (IsEqual(3.5, implantOcclusal)) {
    print("D = 3.5");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.5, 9.0, 10.5, 12.5, 15.5], [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.0, 8.5, 10.5, 13.5], [1.0, 1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.0, 9.0, 12], [1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.5, 10.5], [1.0, 1.0, 1.0]);

    } else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 8.5], [1.0, 1.0]);

    } else if (IsEqual(18.0, implantLength)) {
        print("L = 18.0");

        setDrillPathParams(0.0, 4.1, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5], [1.0]);

    }
} else if (IsEqual(4.3, implantOcclusal)) {
    print("D = 4.3");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(0.0, 5.05, 0.2, 0.25, 0.6, 3.5);
        setSleeveOffsets([5.5, 7.5, 9.0, 10.5, 12.5, 15.5], [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(0.0, 5.05, 0.2, 0.25, 0.6, 3.5);
        setSleeveOffsets([5.5, 7.0, 8.5, 10.5, 13.5], [1.0, 1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(0.0, 5.05, 0.2, 0.25, 0.6, 3.5);
        setSleeveOffsets([5.5, 7.0, 9.0, 12.0], [1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(0.0, 5.05, 0.2, 0.25, 0.6, 3.5);
        setSleeveOffsets([5.5, 7.5, 10.5], [1.0, 1.0, 1.0]);
        
    } else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0.0, 5.05, 0.2, 0.25, 0.6, 3.5);
        setSleeveOffsets([5.5, 8.5], [1.0, 1.0]);

    } else if (IsEqual(18.0, implantLength)) {
        print("L = 18.0");

        setDrillPathParams(0.0, 5.05, 0.2, 0.25, 0.6, 3.5);
        setSleeveOffsets([5.5], [1.0]);
    }
} else if (IsEqual(5.0, implantOcclusal)) {
    print("D = 5.0");

    if (IsEqual(8.0, implantLength)) {
        print("L = 8.0");

        setDrillPathParams(0.0, 6.2, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.5, 9.0, 10.5, 12.5, 15.5], [1.0, 1.0, 1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(10.0, implantLength)) {
        print("L = 10.0");

        setDrillPathParams(0.0, 6.2, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.0, 8.5, 10.5, 13.5], [1.0, 1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(11.5, implantLength)) {
        print("L = 11.5");

        setDrillPathParams(0.0, 6.2, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.0, 9.0, 12.0], [1.0, 1.0, 1.0, 1.0]);

    } else if (IsEqual(13.0, implantLength)) {
        print("L = 13.0");

        setDrillPathParams(0.0, 6.2, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 7.5, 10.5], [1.0, 1.0, 1.0]);

    } else if (IsEqual(15.0, implantLength)) {
        print("L = 15.0");

        setDrillPathParams(0.0, 6.2, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5, 8.5], [1.0, 1.0]);

    } else if (IsEqual(18.0, implantLength)) {
        print("L = 18.0");

        setDrillPathParams(0.0, 6.2, 0.2, 0.2, 0.58, 3.5);
        setSleeveOffsets([5.5], [1.0]);
    }

} else {
    print("Not in library!");
}


// setting sleeve offsets


//Setting Output in the logic object to be used in the application
    logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
    logicData.setSleeveLength(SleeveLength);
    logicData.setSleeveThickness(SleeveThickness);
    logicData.setShelfLength(SleeveShelfHeight);
    logicData.setShelfWidth(SleeveShelfWidth);
    logicData.setAddedMaterialThickeness(AddedMaterialThickness);
    logicData.setDrillingPathDiameter(DrillPathDiameter);
    logicData.setAddDrillLength(AddDrillLength);
    logicData.setSucceedStatus(true);
    logicData.setToolThickness(AddTool);
    

// Equality function.
    function IsEqual(value, diameter) { // I use toFixed function to round the decimal value to two places.
        value = value.toFixed(2);
        diameter = diameter.toFixed(2);
        return (value - diameter) == (0.0).toFixed(2);
    }

// Set DrillPath parameters in one func call.
function setDrillPathParams(addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength) { // Repeated logic added in a function.
    AddDrillLength = addDrill;
    DrillPathDiameter = dPDiameter;
    SleeveInnerDiameter = DrillPathDiameter;
    SleeveShelfWidth = ShWidth;
    SleeveShelfHeight = ShHeight;
    SleeveThickness = SlThickness;
    SleeveLength = SlLength;

    print("Add drill length");
    print(AddDrillLength);
}

// Set offsets in one function call.
function setSleeveOffsets(offsets, tools) {
    for (i = 0; i < offsets.length; i++) {
        logicData.addSleeveOffsetAndTool(offsets[i], tools[i]);
    }
}
