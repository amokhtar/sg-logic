// New Format v.1.0

//Pilot-Nobel-Tapered Logic (Getting input)
var drillingPathType	= logicData.getDrillinPathType();
var guideType 			= logicData.getGuideType();
var implantLength 		= logicData.getImplantLength();
var implantOcclusal 	= logicData.getImplantOcclusalDiameter();
var depthConrtol		= logicData.IsDepthControlOn();
var familyName			= logicData.getFamilyName();
var SleeveInnerDiameter = implantOcclusal;
var implantApical = logicData.getImplantApicalDiameter();


//Logic (Setting output)
var SleeveLength = 1.0;
var SleeveThickness = 1.0;
var SleeveShelfHeight = 0.0;
var SleeveShelfWidth = 0.0;
var AddedMaterialThickness = 1.0;
var AddDrillLength = 0.0;
var DrillPathDiameter = 1.0;
// var AddTool = 0.0;

var stri = familyName.toString().toLowerCase();

// Family name array
var families = [];


// 	Setting different parameters

//if ((stri.indexOf("tapered") > -1)) {
//    print("=> Tapered Implant");
//}
print("\n");

setDrillPathParams(0.0, 2.1, 0.25, 0.25, 0.60, 4.0); // addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength


if (IsEqual(8.0, implantLength)) {
    print("L = 8.0");
    
    setSleeveOffsets([6.0, 8.0, 9.5, 11.0, 14.0], [0.0, 0.0, 0.0, 0.0, 0.0]);

} else if (IsEqual(10.0, implantLength)) {

	setSleeveOffsets([4.0, 6.0, 7.5, 9.0, 12.0], [0.0, 0.0, 0.0, 0.0, 0.0]);

} else if (IsEqual(11.5, implantLength)) {

	setSleeveOffsets([2.5, 4.5, 6.0, 7.5, 10.5], [0.0, 0.0, 0.0, 0.0, 0.0]);

} else if (IsEqual(13.0, implantLength)) {

	setSleeveOffsets([1.0, 3.0, 4.5, 6.0, 9.0], [0.0, 0.0, 0.0, 0.0, 0.0]);

} else if (IsEqual(16.0, implantLength)) {

	setSleeveOffsets([1.5, 3.0, 6.0], [0.0, 0.0, 0.0]);

} else {
    print("Not in library!");
}


// setting sleeve offsets


//Setting Output in the logic object to be used in the application
logicData.setSleeveInnerDiameter(SleeveInnerDiameter);
logicData.setSleeveLength(SleeveLength);
logicData.setSleeveThickness(SleeveThickness);
logicData.setShelfLength(SleeveShelfHeight);
logicData.setShelfWidth(SleeveShelfWidth);
logicData.setAddedMaterialThickeness(AddedMaterialThickness);
logicData.setDrillingPathDiameter(DrillPathDiameter);
logicData.setAddDrillLength(AddDrillLength);
logicData.setSucceedStatus(true);
logicData.setToolThickness(AddTool);

// Equality function.
function IsEqual(value, diameter) { // I use toFixed function to round the decimal value to two places.
    value = value.toFixed(2);
    diameter = diameter.toFixed(2);
    return (value - diameter) == (0.0).toFixed(2);
}

// Set DrillPath parameters in one func call.
function setDrillPathParams(addDrill, dPDiameter, ShWidth, ShHeight, SlThickness, SlLength) { // Repeated logic added in a function.
    AddDrillLength = addDrill;
    DrillPathDiameter = dPDiameter;
    SleeveInnerDiameter = DrillPathDiameter;
    SleeveShelfWidth = ShWidth;
    SleeveShelfHeight = ShHeight;
    SleeveThickness = SlThickness;
    SleeveLength = SlLength;

    print("Add drill length");
    print(AddDrillLength);
}

// Set offsets in one function call.
function setSleeveOffsets(offsets, tools) {
    for (i = 0; i < offsets.length; i++) {
        logicData.addSleeveOffsetAndTool(offsets[i], tools[i]);
    }
}
